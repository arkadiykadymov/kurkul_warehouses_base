package kurkul.wh.base.base.dto;

import kurkul.wh.base.base.models.ErrorModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseDTO<T> {

    private T data;
    private ErrorModel error;

    public BaseDTO(T data) {
        this.data = data;
    }

    public BaseDTO(ErrorModel error) {
        this.error = error;
    }
}
