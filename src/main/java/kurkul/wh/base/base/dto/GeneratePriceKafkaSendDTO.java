package kurkul.wh.base.base.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GeneratePriceKafkaSendDTO {

    private Long id;
    private BigDecimal price;
}
