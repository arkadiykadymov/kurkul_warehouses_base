package kurkul.wh.base.base.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum ErrorCode {
    GOOD_NAME_NULL("Exception.goodNameNull"),
    GOOD_NOT_FOUND("Exception.goodNotFound"),
    NOT_ENOUGH_SPACE("Exception.notEnoughPlace"),
    INTERNAL_ERROR("Exception.internalError"),
    QUANTITY_ROW_SHELF_NULL("Exception.quantityRowShelfNullError"),
    VALIDATION_ERROR("Exception.validationError"),
    NO_SUCH_WP("Exception.noSuchWp");

    private String desc;
}